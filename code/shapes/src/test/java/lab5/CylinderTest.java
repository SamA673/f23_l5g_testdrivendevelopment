// Samin Ahmed 2043024
package lab5;

import static org.junit.Assert.*;
import org.junit.Test;

public class CylinderTest {
    @Test
    public void testGetSurfaceArea() {
        double radius = 10.00;
        double height = 12.00; 
        Cylinder c = new Cylinder(radius, height);
        assertEquals(1382.300, c.getSurfaceArea(), 0.01); 
    }

    @Test
    public void testGetVolume() {
        double radius = 10.00;
        double height = 12.00; 
        Cylinder c = new Cylinder(radius, height);
        assertEquals(3769.91, c.getVolume(), 0.01); 
    }

    @Test
    public void testConstructorWithNegativeRadius() {
        try {
            double radius = -10.00; 
            double height = 12.00; 
            Cylinder c = new Cylinder(radius, height);
            fail("Expected an IllegalArgumentException to be thrown, but it was not.");
        } catch (IllegalArgumentException e) {
            // Failed successfully
        }
    }

    @Test
    public void testConstructorWithValidValues() {
        double validRadius = 5.0; 
        double height = 10.0; 
        Cylinder cylinder = new Cylinder(validRadius, height);
        assertEquals(validRadius, cylinder.getRadius(), 0.001); // Must add getRadius getter first...
        assertEquals(height, cylinder.getHeight(), 0.001); // Must add getHeight getter first...
    }





}
