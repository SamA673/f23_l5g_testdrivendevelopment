// David Parker 2139469
package lab5;

import static org.junit.Assert.*;
import org.junit.Test;

public class SphereTest {
    @Test
    public void testGetRadius_ReturnsPassedValues() {
        double radius = 2.5;
        Sphere sphere = new Sphere(radius);
        assertEquals(radius, sphere.getRadius(), 0);
    }

    @Test
    public void testConstructor_RadiusNegative() {
        try {
            double radius = -10;
            Sphere sphere = new Sphere(radius);
            fail("Exception was expected for a negative value");
        } catch (IllegalArgumentException e) {
            // Failed Successfully
        }
    }

    @Test
    public void testGetVolume() {
        double radius = 5;
        Sphere sphere = new Sphere(radius);
        assertEquals(523.598, sphere.getVolume(), 0.001);
    }

    @Test
    public void testGetSurfaceArea() {
        double radius = 5;
        Sphere sphere = new Sphere(radius);
        assertEquals(314.159, sphere.getSurfaceArea(), 0.001);
    }
}
