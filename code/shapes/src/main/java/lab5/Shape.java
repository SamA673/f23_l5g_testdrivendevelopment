package lab5;

/** 
 *  Shape is an interface used to hold methods for
 *  3D shapes like Spheres and Cylinders!
 *  @author Samin Ahmed
 *  @version 3/10/2023
 */
public interface Shape
{
    double getVolume();
    double getSurfaceArea();
}   
