// David Parker 2139469
package lab5;

/**
 * Cylinder is used to contain cylinder information
 * and calculations
 * @author David Parker
 * @version 10/3/2023
 */
public class Cylinder implements Shape {
    private double radius;
    private double height;

    /**
     * The constructor for Cylinder class
     * @param radius Radius of cylinder
     * @param height Height of cylidner
     * @throws IllegalArgumentException Throws exception if radius or height is negative
     */
    public Cylinder(double radius, double height) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius cannot be negative.");
        }
        if (height < 0) {
            throw new IllegalArgumentException("Height cannot be negative.");
        }
        this.radius = radius;
        this.height = height;
    }

    /**
     * Returns the radius of the cylinder
     * @return Radius of cylinder
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Returns the height of the cylinder
     * @return Height of the cylinder
     */
    public double getHeight() {
        return height;
    }

    /**
     * Returns the volume of the cylinder object
     * 
     * @return The volume of the cylinder
     */
    public double getVolume() {
        return (Math.PI * Math.pow(this.radius, 2) * this.height);
    }

    /**
     * Returns the surface area of the cylinder object
     * 
     * @return The surface area of the cylinder
     */
    public double getSurfaceArea() {
        return (2*Math.PI*Math.pow(this.radius, 2)) + (2*Math.PI*this.radius*this.height);
    }
}
