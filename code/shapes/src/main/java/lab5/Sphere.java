// Samin A 2043024
package lab5;

/** 
 *  Sphere is a class used for a sphere object
 *  which holds information about a sphere.
 *  @author Samin Ahmed
 *  @version 3/10/2023
 */
public class Sphere implements Shape {
    private double radius; 

    /** 
     * Constructs a sphere 
     * @param radius
     * @throws IllegalArgumentException If radius is negative
     */
    public Sphere(double radius){
        if (radius < 0) {
            throw new IllegalArgumentException("Radius cannot be negative");
        }
        this.radius = radius; 
    }

    /**
     * Gets the radius for the current sphere
     * @return radius of sphere
     */
    public double getRadius(){
        return this.radius; 
    }

    /**
     * Gets the volume of a sphere using
     * mathematical formula.
     * @return The volume of this Sphere
     */
    public double getVolume(){
        return (4.0/3.0)* Math.PI * (radius * radius * radius);
    }

    /**
     * Gets the surface area of a sphere using 
     * mathematical formula.
     * @return The surface area of a sphere
     */
    public double getSurfaceArea(){
        return (4.0 * Math.PI * this.radius * this.radius);
    }

}
